import platform
import socket
import sys
import os
import nmap3
import pyfiglet
from tabulate import tabulate


s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.settimeout(5)
ascii_banner = pyfiglet.figlet_format("Fast Sanner ")
print(ascii_banner)
print("\n****************************************************************")
print("\n*              Copyright of Black_hat___, 2022                 *")
print("\n****************************************************************") 
if os.geteuid() != 0:
    exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")

print()


data = [["Port Scanner", 1], 
        ["Os Scanner", 2], 
        ["DNS Scanner", 3], 
        ["SQL Injection Scanner", 4]]
  
#define header names
col_names = ["Features", "Selection Number"]
  
#display table
print(tabulate(data, headers=col_names, tablefmt="fancy_grid", showindex="always"))

#Port Scanner
def portScanner(portx,porty): 
    for x in range(portx,porty):
        y=x
        if s.connect_ex((host, y)):
         print("The" "port",y, "is closed")
        else:
         print("The" "port",y, "is open")
         s.close()

#Os Scanner 
def Os_Scanner(host):
    nmap = nmap3.Nmap()
    os_results = nmap.nmap_os_detection(host)
    print(os_results)

##DNS Scanner
def Dns_Scanner(host):
    nmap = nmap3.Nmap()
    results = nmap.nmap_dns_brute_script("your-host.com")
    print(results)

##Fin Scaner
def Fin_Scanner(host):
    nmap = nmap3.NmapScanTechniques()
    result = nmap.nmap_fin_scan(host)
    print(result)


#Selection numbers
while (True):
    Num1=int(input("Enter Selection Number: "))
    if Num1==1:
        host= input("Enter ip address: ")
        portx=int(input("Enter port range:"))
        porty=int(input("Enter port range:"))
        portScanner(portx,porty)
        continue
    elif Num1==2:
        host= input("Enter ip address: ")
        Os_Scanner(host)
        continue
    elif Num1 == 3:
        host= input("Enter Domain Name: ")
        Dns_Scanner(host)
        continue
    elif Num1==4:
         
         exit()



   









 
    

